from __future__ import absolute_import
from .api.exchange import ExchangeInterface
from . import settings

MT4_LOT_TO_BITMEX_MULTIPLIER = {
    'BTCUSD': 100000,
    'BTCUSDmicro': 1000,
    'EURUSD': 100000
}

class BitMEX(ExchangeInterface):
    def get_exposure(self):
        return self.get_position()

    # def get_ticker():
    #     inherited from ExchangeInterface

    def _send_market(self, qty, side):
        return self.send_market([{
            'symbol': settings.SYMBOL,
            'orderQty': qty,
            'side': side
        }])

    def buy(self, symbol, mt4Qty):
        qty = MT4_LOT_TO_BITMEX_MULTIPLIER[symbol] * mt4Qty
        return self._send_market(qty, 'Buy')

    def sell(self, symbol, mt4Qty):
        qty = MT4_LOT_TO_BITMEX_MULTIPLIER[symbol] * mt4Qty
        return self._send_market(qty, 'Sell')

    def run_loop(self):
        while True:
            self.run()

    def __init__(self):
        super().__init__()

if __name__ == '__main__':
    # bitmex = BitMEX()
    # print(bitmex.sell(3000))
    # print(bitmex.get_ticker())
    # print(bitmex.get_exposure())
    pass
