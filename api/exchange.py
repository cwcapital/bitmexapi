from __future__ import absolute_import
import requests
import atexit
import signal
import logging
from time import sleep
from .utils import setup_custom_logger
from . import bitmex
from .. import settings

logger = setup_custom_logger('root')

class ExchangeInterface:
    def __init__(self, dry_run=False):
        self.dry_run = dry_run
        self.symbol = settings.SYMBOL
        orderIDSuffix = {
            'ORDERID_RECONCILIATION_SUFFIX': '',
            'ORDERID_RECONCILIATION_HEDGE': ''
        }
        self.bitmex = bitmex._BitMEX(base_url=settings.BASE_URL, symbol=self.symbol, apiKey=settings.API_KEY,
                                    apiSecret=settings.API_SECRET, orderIDPrefix='',
                                    orderIDSuffix=orderIDSuffix)

    def cancel_order(self, order):
        logger.info("Cancelling: %s %d @ %.2f" % (order['side'], order['orderQty'], "@", order['price']))
        while True:
            try:
                self.bitmex.cancel(order['orderID'])
                sleep(1)
            except ValueError as e:
                logger.info(e)
                sleep(1)
            else:
                break

    def fetch_user_margin(self):
        return self.bitmex.fetch_user_margin()

    def send_market(self, orders):
        if self.dry_run:
            return

        result = None

        # In certain cases, a WS update might not make it through before we call this.
        # For that reason, we grab via HTTP to ensure we grab them all.
        if len(orders):
            for order in orders:
                logger.info("Sending market order: %s %d" % (order['side'], order['orderQty']))

            result = self.bitmex.send_market(orders)

        sleep(1)
        return result

    def cancel_all_orders(self, side=None):
        # side = { 'Buy', 'Sell', None }

        if self.dry_run:
            return

        logger.info("Resetting current position. Cancelling all existing orders.")

        # In certain cases, a WS update might not make it through before we call this.
        # For that reason, we grab via HTTP to ensure we grab them all.
        orders = self.bitmex.http_open_orders()
        if side:
            orders = [o for o in orders if o['side'] == side]

        for order in orders:
            logger.info("Cancelling: %s %d @ %.2f" % (order['side'], order['orderQty'], order['price']))

        if len(orders):
            self.bitmex.cancel([order['orderID'] for order in orders])

        sleep(1)

    def get_margin(self):
        if self.dry_run:
            return {'marginBalance': float(settings.DRY_BTC), 'availableFunds': float(settings.DRY_BTC)}
        return self.bitmex.funds()

    def get_orders(self):
        if self.dry_run:
            return []
        return self.bitmex.open_orders()

    def get_position(self, symbol=None):
        if symbol is None:
            symbol = self.symbol
        return self.bitmex.position(symbol)

    def get_ticker(self, symbol=None):
        if symbol is None:
            symbol = self.symbol
        return self.bitmex.ticker_data(symbol)

    def is_open(self):
        """Check that websockets are still open."""
        return not self.bitmex.ws.exited

    def check_market_open(self):
        instrument = self.get_instrument()
        if instrument["state"] != "Open" and instrument["state"] != "Closed":
            raise errors.MarketClosedError("The instrument %s is not open. State: %s" %
                                           (self.symbol, instrument["state"]))
            sys.exit()

    def check_if_orderbook_empty(self):
        """This function checks whether the order book is empty"""
        instrument = self.get_instrument()
        if instrument['midPrice'] is None:
            raise errors.MarketEmptyError("Orderbook is empty, cannot quote")
            sys.exit()

    def amend_bulk_orders(self, orders):
        if self.dry_run:
            return orders
        return self.bitmex.amend_bulk_orders(orders)

    def create_bulk_orders(self, orders, suffix=""):
        if self.dry_run:
            return orders

        return self.bitmex.create_bulk_orders(orders, suffix)

    def cancel_bulk_orders(self, orders):
        if self.dry_run:
            return orders
        return self.bitmex.cancel([order['orderID'] for order in orders])


class OrderManager:
    def __init__(self):
        self.exchange = ExchangeInterface(dry_run=False)
        # Once exchange is created, register exit handler that will always cancel orders
        # on any error.
        atexit.register(self.exit)
        signal.signal(signal.SIGTERM, self.exit)

        logger.info("Using symbol %s." % self.exchange.symbol)

    def init(self):
        if settings.DRY_RUN:
            logger.info("Initializing dry run. Orders printed below represent what would be posted to BitMEX.")
        else:
            logger.info("Order Manager initializing, connecting to BitMEX. Live run: executing real trades.")

        self.start_time = datetime.now()
        self.instrument = self.exchange.get_instrument()
        self.starting_qty = self.exchange.get_delta()
        self.running_qty = self.starting_qty
        self.reset()

        strategy.init(self)

    def reset(self):
        self.exchange.cancel_all_orders()
        self.sanity_check()
        self.print_status()

    def sanity_check(self):
        """Perform checks before placing orders."""

        # Check if OB is empty - if so, can't quote.
        self.exchange.check_if_orderbook_empty()

        # Ensure market is still open.
        self.exchange.check_market_open()


    def print_status(self):
        """Print the current MM status."""
        pass

    def exit(self):
        logger.info("Shutting down. All open orders will be cancelled.")
        try:
            self.exchange.cancel_all_orders()
            strategy.deinit(self)
            self.exchange.bitmex.exit()
        except errors.AuthenticationError as e:
            logger.info("Was not authenticated; could not cancel orders.")
        except Exception as e:
            logger.info("Unable to cancel orders: %s" % e)

        sys.exit()

    def reset(self):
        self.sanity_check()
        self.print_status()

    def init(self):
        if settings.DRY_RUN:
            logger.info("Initializing dry run. Orders printed below represent what would be posted to BitMEX.")
        else:
            logger.info("Order Manager initializing, connecting to BitMEX. Live run: executing real trades.")

        self.start_time = datetime.now()
        self.instrument = self.exchange.get_instrument()
        self.reset()

    def run(self):
        sys.stdout.write("-----\n")
        sys.stdout.flush()

        sleep(settings.LOOP_INTERVAL)

        # This will restart on very short downtime, but if it's longer,
        # the MM will crash entirely as it is unable to connect to the WS on boot.
        if not self.check_connection():
            logger.error("Realtime data connection unexpectedly closed, restarting.")
            self.restart()

        self.sanity_check()  # Ensures health of mm - several cut-out points here
        self.print_status()  # Print skew, delta, etc

    def get_ticker(self):
        ticker = self.exchange.get_ticker()
        return ticker
